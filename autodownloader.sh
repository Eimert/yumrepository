#!/bin/bash
# created 13-04-18 in my private dev time
# Declare
# get new PIVOTAL_API_TOKEN from network.pivotal.io
# set as secret var in gitlab
# Untilizes HATEOAS.
DEST_DIR="./repodir"
YAML_PARSER="parse_yaml.sh"
AUTODOWNLOADER_CONFIG="config.yml"
usage() {
  echo -en "Usage: $0 [options]\n\nOptions:\n  --download\tFetch packages to dest dir.\n\n  --createrepo\tInvoke createrepo command on $DEST_DIR.\n\nexample:\n$0 --download\n"
  exit 1
}

check_requirements() {
  if [[ ! $(jq --version) ]] ; then echo "install jq>=1.5" ; exit 2 ; fi
  if [[ ! $(yum --version) ]] ; then echo "install yum" ; exit 2 ; fi
  if [[ ! $(createrepo --version) ]] ; then echo "install createrepo" ; exit 2 ; fi
  if [[ ! -d "$DEST_DIR" ]] ; then mkdir -pv "$DEST_DIR" ; fi
  if [[ ! -f "$YAML_PARSER" ]] ; then echo "missing $YAML_PARSER" ; exit 2 ; fi
  if [[ ! -f "$AUTODOWNLOADER_CONFIG" ]] ; then echo "missing $AUTODOWNLOADER_CONFIG" ; exit 2 ; fi
}

source_env() {
  . .env
}

if [[ "$1" == "--help" ]] || [[ "$1" == "--usage" ]] || [[ $# -lt 1 ]] ; then
  usage
fi

for var in "$@" ; do
  case "$var" in
    --download)
      DOWNL="true" ;;
    --usage)
      usage ;;
    --help)
      usage ;;
    --version)
      echo "$0 v0.2" ; exit 0 ;;
    --createrepo)
      CREATEREPO="true" ;;
    *)
      echo "Unknown argument" ; usage ;
  esac ;
  shift ;
done

check_requirements
source "$YAML_PARSER"
# read confilg file. Access example: $config_tcserver4_name
eval $(parse_yaml "$AUTODOWNLOADER_CONFIG" "config_")
exec 6>&1 # redirect fd 6 to stdout

if [[ -z "${PIVOTAL_API_TOKEN}" ]] ; then source_env || echo "var PIVOTAL_API_TOKEN not set." || exit 3 ; fi
# Exchange an UAA refresh token for a temporary (1h) UAA access token.
ACCESS_TOKEN=$(curl --silent --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 \-X POST -d "{\"refresh_token\": \"${PIVOTAL_API_TOKEN}\"}" https://network.pivotal.io/api/v2/authentication/access_tokens | jq --raw-output '.access_token')
if [[ -z "${ACCESS_TOKEN}" ]] || [[ "${ACCESS_TOKEN}" == "null" ]] ; then echo "var ACCESS_TOKEN empty or 'null'." ; exit 5 ; fi
echo "export ACCESS_TOKEN=$ACCESS_TOKEN" > setAccessToken.sh

# Loop trough all keys in config file
for key in $(echo "${!config_*}" | tr " " "\n" | grep -oE "^[a-z]+_[a-z]+[0-9]?" | uniq) ; do
  export name=$(eval echo "$"$key"_name")
  export version=$(eval echo "$"$key"_version")
  export revisions=$(eval echo "$"$key"_revisions")
  export releases_url=$(eval echo "$"$key"_releases_url")
  echo "name=$name version=$version revisions=$revisions"
  if [[ -z "${name}" ]] ; then echo "error parsing $AUTODOWNLOADER_CONFIG" ; exit 2 ; fi
  if [[ $revisions -eq 0 ]]; then continue ; fi

  key_release_urls() {
      KEY_RELEASE_URLS=$(curl --silent --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 -H "Authorization: Bearer $ACCESS_TOKEN" $releases_url | \
      jq --raw-output '.releases[] | select(.version | test(env.version)) | ._links.self.href ' 2>/dev/null | head -$revisions | tee /dev/fd/6)
      if [[ -z "${KEY_RELEASE_URLS}" ]] || [[ "${KEY_RELEASE_URLS}" == "null" ]] ; then echo "var KEY_RELEASE_URLS empty or 'null'." ; exit 6 ; fi
  }
  key_release_urls


  for RELEASE_URL in $KEY_RELEASE_URLS ; do
    # For every release URL, get RPM filename and Download URL

    RPM_FILENAME=$(curl --silent --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 -H "Authorization: Bearer $ACCESS_TOKEN" $RELEASE_URL | \
      jq --raw-output '.file_groups[].product_files[] | '.aws_object_key' | match(env.name) | .string' 2>/dev/null | tee /dev/fd/6)

    if [[ -z "${RPM_FILENAME}" ]] || [[ "${RPM_FILENAME}" == "null" ]] && [[ "$name" == *"-runtime-"* ]] ; then
      echo "No tomcat runtimes in this release. Incremeting revisions to lookup a 4.x release that includes tomcat runtimes."
      revisions+=$((revisions+1))
      key_release_urls
      continue # restart loop
    fi

    if [[ -z "${RPM_FILENAME}" ]] || [[ "${RPM_FILENAME}" == "null" ]] ; then
      echo "$RELEASE_URL var RPM_FILENAME empty or 'null': $RPM_FILENAME" ; exit 7 ;
    fi

    DOWNLOAD_URL=$(curl --silent --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 -H "Authorization: Bearer $ACCESS_TOKEN" $RELEASE_URL | \
      jq --raw-output '.file_groups[].product_files[] | select(.aws_object_key | test(env.name)) | '._links.download.href'' 2>/dev/null | tee /dev/fd/6)
    if [[ -z "${DOWNLOAD_URL}" ]] || [[ "${DOWNLOAD_URL}" == "null" ]] ; then echo "var DOWNLOAD_URL empty or 'null'." ; exit 8 ; fi

    if [[ "$DOWNL" ]] ; then
      paste <(echo "$RPM_FILENAME") <(echo "$DOWNLOAD_URL") | while read RPM_FILENAME DOWNLOAD_URL; do
        echo "Downloading $RPM_FILENAME $DOWNLOAD_URL" ;
        if [[ -s "$DEST_DIR/$RPM_FILENAME" ]] ; then echo -en "File already exists: $DEST_DIR/$RPM_FILENAME\n" ; break ; fi
        curl --silent --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 -L -H "Authorization: Bearer $ACCESS_TOKEN" -o "$DEST_DIR/$RPM_FILENAME" "$DOWNLOAD_URL" ;
        rpm -Uvh --test --nodeps "$DEST_DIR/$RPM_FILENAME" || exit 9;
      done
      else
        echo "Done. Not downloading packages. Use option --download"
    fi
  done # end of download
done # end of config loop
ls -l "$DEST_DIR"

if [[ "$CREATEREPO" ]] ; then
  createrepo "$DEST_DIR"
fi

