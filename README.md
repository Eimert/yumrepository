yumrepository [![Build Status](https://gitlab.com/Eimert/yumrepository/badges/master/build.svg)](https://gitlab.com/Eimert/yumrepository)
=============
<img src="https://upload.wikimedia.org/wikipedia/commons/0/0e/Yum.png" align="right">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/RPM_Logo.svg/1280px-RPM_Logo.svg.png" width="14%" align="right">
Repository for pivotal tc-server RPMs.<br>

## Usage
Script is intended to be ran in a pipeline, publishing (pivotal) .rpm [artefacts](./config.yml) on [heroku](https://yumrepository.herokuapp.com).

> The free Heroku dyno can be an bit slow when it is resuming from the sleeping state.

## Local usage

`git clone` This repo. Get (your) UAA API token @ account page on [network.pivotal.io](https://network.pivotal.io/). `export PIVOTAL_API_TOKEN=`

Run the autodownloader script, pulling rpms:
```script
./autodownloader.sh --download --createrepo
```
For easy debugging curl commands, use `./autodownloader.sh ; . setAccessToken.sh` to get and export the latest `$REFRESH_TOKEN` (valid for ~1h).
[Pivotal API Auth reference](https://network.pivotal.io/docs/api).

## Running the yumdownloader flask site locally:
```bash
eimert@EIM yumrepository $ . yumrepository/bin/activate
(yumrepository) eimert@EIM yumrepository $ pip install -r requirements.txt
...
(yumrepository) eimert@EIM yumrepository $ python hello.py
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 121-468-897
```

## Update python version
Watch heroku logs for updates.
```bash
heroku logs --tail -a yumrepository
```
Update version in [runtime.txt](./runtime.txt).

## Update pip requirements.txt
```bash
eimert@EIM yumrepository $ . yumrepository/bin/activate
sudo chown -R eimert.eimert yumrepository/
pip install --upgrade pip
pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U
pip freeze > requirements.txt
git add requirements.txt
git commit -m "updated pip dependencies."
```
