from flask import Flask, render_template, abort, send_file
from flask_bootstrap import Bootstrap
import os, time, platform
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

app = Flask(__name__)

bootstrap = Bootstrap(app)

def creation_date(path_to_file):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime

@app.context_processor
def inject_deploy_time():
    epoch = creation_date("repodir/repodata/repomd.xml")
    return dict(build_time=(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch))))

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500

@app.route('/')#, defaults={'req_path': ''})
def index():
    return render_template('index.html')

@app.route('/<path:req_path>')
def dir_listing(req_path):
    BASE_DIR = './'

    # Joining the base and the requested path
    abs_path = os.path.join(BASE_DIR, req_path)

    # Return 404 if path doesn't exist
    if not os.path.exists(abs_path):
        return abort(404)

    # Check if path is a file and serve
    if os.path.isfile(abs_path):
        return send_file(abs_path)

    # Show directory contents
    files = []
    base_filenames = os.listdir(abs_path)
    for base_filename in base_filenames:
        files.append(os.path.join(abs_path, base_filename))
    # files =
    return render_template('files.html', files=files, header=req_path)

@app.route('/user/<name>')
def user(name):
    return render_template('user.html', name=name)

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
